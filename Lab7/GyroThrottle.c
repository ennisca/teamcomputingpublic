task main()
{

	int gyroRate = 0;
	SensorType[S4] = sensorEV3_Gyro;
	displayBigTextLine(1,"Lab Demo GyroThrottle");   // Useful command but not required

	sleep(1000);
	resetGyro(S4);
	sleep(2000);


	// The code below will turn the motor proportionally to the gyro rate
	while (true)
	{
		// Read the current rate value in degrees/second
		gyroRate = getGyroHeading(S4);

		displayBigTextLine(2,"Heading= %d",gyroRate);

		// Clip the value of the gyro to -100 to +100 (min and max motor speeds)
		if (gyroRate > 100)
			gyroRate = 100;
		else if (gyroRate < -100)
			gyroRate = -100;

		// Set motor speeds to the rate of the gyro
		setMotorSync(motorB, motorC, 0, gyroRate);

		sleep(10);
	}
}
