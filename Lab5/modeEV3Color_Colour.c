
task main()
{
	
	TLegoColors color;
	
	SensorType[S3]  = sensorEV3_Color;
	SensorMode[S3]  = modeEV3Color_Color;
	
	setLEDColor(ledOff);
	
	
	while (true)
	{
		color = getColorName(S3);
		displayBigTextLine(4, "Color: %d", color);
		if (color == colorBlue)
			displayBigTextLine(6, "Color is: BLUE");
		else if (color == colorRed)
			displayBigTextLine(6, "Color is: RED");
		else if (color == colorBlack)
			displayBigTextLine(6, "Color is: BLACK");
		else if (color == colorYellow)
			displayBigTextLine(6, "Color is: YELLOW");
		else if (color == colorWhite)
			displayBigTextLine(6, "Color is: White");
		else
			displayBigTextLine(6, "Color num is: %d", color);
		
		sleep(2000);
	}
}
